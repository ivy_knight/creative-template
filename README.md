# creative-template #
Template for the Creative team to clone into project repositories (e.g. services-app-customer-creative).
* * *
## Getting Started ##
1. This is your creative repo for any deliverables you will need to pass to the project's developers. Make sure you are commiting to the correct project repo. Creative repos are following the ProjectName-creative naming convention. 
2. Clone the repo by clicking the "Clone in SourceTree" icon. 
3. This will open SourceTree and prompt to select a location and name to Clone As. *Make sure to have a local PROJECTS folder to keep your folder locally, DO NOT CLONE ONTO DROPBOX*
3. The Project will be located under GitHub in the left rail. Right click and Open in Finder. The directory structure should be ready to add deliverables.

## Updating Deliverables ##
1. In Finder, copy files into the appropriate folders.
2. Back in SourceTree, under the File Status, the updated files will appear in Unstaged files. Check Unstaged files to move them to stage.
3. Add a Commit message. Check Push changes immediately to origin/master. Hit Commit.
4. Repeat steps 1–3, whenever you need to update deliverables.
* * *
## Guidelines ##
### Directory Structure: ###
**iOS**

* 01_wires
* 02_redlines
* 03_assets (include all assets and app icons)
    * @ 1x
    * @ 2x  
    * @ 3x

*For App Framework (Solutions based apps) assets need to contain the framework and module prefix:
   * appframe_ _[module]_[name].png 
   * ex: appframe__directory_icon_navigate.png*

**Android**

* 01_wires
* 02_redlines
* 03_assets (include all assets, launcher, and notification icons)
    * drawable-hdpi
    * drawable-mdpi
    * drawable-xhdpi
    * drawable-xxhdpi
    * drawable-xxxhdpi
    * mipmap-hdpi
    * mipmap-mdpi
    * mipmap-xdpi
    * mipmap-xxdpi
    * mipmap-xxxdpi

*For App Framework (Solutions based apps) assets need to contain the framework and module prefix:
   * appframe__[module]_[drawable_name].png 
   * ex: appframe__directory_ic_navigate.png 



*Consult with developer, if they have a preference for the project.* 





* * *
## Naming Conventions ##
**Wireframe:** XX_platform_device_wireframe.pdf

**Redlines:** XX_platform_device_redlines.pdf

**iOS Assets** 

* icons:        *icon_name.png* 
* backgrounds:  *bg_name.png* 
* nav bar:      *icon_navbar_name.png* 
* menu:         *icon_nav_name.png* 
* placeholder:  *placeholder_name.png* 
* splash:       *splash.png* 
* buttons:      *btn_name.png* 
* other:        *item_component_name.png* (ex: “icon_player_playhead.png”)

**Android Assets** 

* icons:        *ic_name.png* 
* backgrounds:  *bg_name.png* 
* action bar:   *ic_action_name.png* 
* menu:         *ic_nav_name.png* 
* placeholder:  *placeholder_name.png* 
* load:         *load.png* (only if needed) 
* buttons:      *btn_name.png* (best to define style in redline) 
* other:        *item_component_name.png*   (ex: “ic_player_playhead.png”)